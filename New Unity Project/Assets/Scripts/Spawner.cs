﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{

    public class Spawner : MonoBehaviour
{
    public int spawnCounter;

    private void Spawning()
    {
        for (int i = 0; i < spawnCounter; i++);
        {
            GameObject newGo = new GameObject();
            Debug.Log(message: "Something spawned");
        }

    }

    public void Start()
    {
        Spawning();
    }

    private void SpawningTwo(int a)
    {
        while (spawnCounter < a)
        {
            GameObject go = new GameObject();

            spawnCounter++;
        }

    }

    private void SpawningThree(int b)
    {
        do
        {
            GameObject go = new GameObject();

            spawnCounter++;
        }
        while (spawnCounter < b);
    }

}
}