﻿using UnityEngine;
using System.Collections;

namespace CoAHomework
{

    public class NumberChecker : MonoBehaviour
{
    public int[] numbers;

    public int numberToSkip = 1;

    public int numberToBreakout = 2;

    private void NumberLoop()
    {
        for (int i = 0; i < numbers.Length; i++)
        {
            Debug.Log(numbers[i]);

            if (i == numbers.Length)
            {
                Debug.Log(message: "The loop is finish and the Method was succesful.");
            }
        }
    }

    private void NumberScoop(int a)
    {
        for (int i = 0; i < numbers.Length; i++)
        {
            if (numbers[i] == a)
            {
                Debug.Log(message: "Skipping");
            }
        }
        Debug.Log(message: "the loop is finished and the method was called successfully");
    }

    private void NumberSwoop(int a, int b)
    {
        for (int i = 0; i < numbers.Length; i++)
        {
            if (numbers[i] == a)
            {
                Debug.Log(message: "Skipping");
            }

            if (numbers[i] == b)
            {
                Debug.Log(message: "Breaking the Loop.");
                break;
            }
            Debug.Log(message: "The loop is finish and the Method was succesful.");

        }
       
    }

    public void Start()
    {
        NumberLoop();
        NumberScoop(4);
        NumberSwoop(4, 8);
    }

}
}
